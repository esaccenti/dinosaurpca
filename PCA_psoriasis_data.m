%%
cd('C:\PROGETTI\PCA_EDUCATION\mfiles')

%% Load data
data = xlsread('Reduced_Psoriasis_data.xlsx');

%% Select class labels
class = data(:,1);

%% Select gene expression values
Xdata = data(:,2:101);

%% Do PCA on standardized data (z-scored)
[Loadings,Scores,latent,~,explained] = pca(zscore(Xdata));

%% Dispaly explained variance
explained(1:3) % for the first three components

%% Create the score plot
figure(1);
hold on;

for i = 0:1

    x = Scores(class==i, 1);
    y = Scores(class==i, 2);
    z = Scores(class==i, 3);
    % Plot the points with the corresponding marker
    hold on
    plot3(x, y,z, '.', 'markersize',25);
end


% Customize the plot add variance explained
xlabel('PC 1 (39.2%)');
ylabel('PC 2 (11.83%)');
zlabel('PC 3 (6.5%)')
legend('Healthy','Diseased');

axis square
set(gcf,'color','w');
H=gca;
H.LineWidth=1; %change to the desired value
H.FontSize = 13;
box off

%% Make loading plot
% Loadings of first Principal Component
bar([1:100],Loadings(:,1),'FaceColor',[192 192 192]/255);
xlabel('Variable (genes)')
ylabel('Loadings 1st PC')
box off
H=gca;
H.LineWidth=1; %change to the desired value     
set(gcf,'color','w');
set(gca,'fontsize',15);
axis square

set(gcf,'color','w');
H=gca;
H.LineWidth=1; %change to the desired value
H.FontSize = 13;
box off
